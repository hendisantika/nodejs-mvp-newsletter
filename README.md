# NodeJS MVP NewsLetter

#### Run this project by these commands :
1. `git clone https://gitlab.com/hendisantika/nodejs-mvp-newsletter.git`
2. `cd nodejs-mvp-newsletter`
3. `npm install`
4. `npm run start`

#### Screen shot

Index Home page

![Index Home Page](img/index.png "Index Home Page")

Subscribe Page

![Subscribe Page](img/subscribe.png "Subscribe Page")
